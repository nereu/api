module.exports = (sequelize, DataTypes) => {
    const Pictograma = sequelize.define('Pictograma', {

        tipo_ES: DataTypes.STRING,
        tipo_EN: DataTypes.STRING,
        tipo_CA: DataTypes.STRING,
        pictograma: DataTypes.STRING,
        audio_ES: DataTypes.STRING,

    }, { tableName: 'tea_tea', timestamps: false });
    return Pictograma;
};
